#include <stdio.h>
#include "../../Library/Kernel.h"


typedef int (*P_INT_ARRAY)[];						/* int型の配列へのポインタにP_INT_ARRAYという別名を付ける */

PRIVATE int m_aArray[5] = { 0, 1, 2, 3, 4 };


PRIVATE int add( int a, int b )
{
	return a + b;
}

PRIVATE int sub( int a, int b )
{
	return a - b;
}

PRIVATE int multi( int a, int b )
{
	return a * b;
}

PRIVATE void printDemo( char* words )
{
	printf("%s\n" ,words );
	return;
}

PRIVATE void ExecNormalFunctionCall( int a, int b, int c )
{
	printDemo("\nNormal Function Call");

	c = add( a, b );
	printf( "a + b = %d\n" ,c );
	c = sub( a, b );
	printf( "a - b = %d\n" ,c );
	c = multi( a, b );
	printf( "a * b = %d\n" ,c );

}

PRIVATE void ExecPointerFunctionCall( int a, int b, int c )
{
	void (*pfunc2)(char*);					/* 返り値がvoid型で、char型ポインタを引数に取る関数へのポインタの宣言 */
	int (*pfunc)(int, int);					/* 返り値がint型で、int型の引数を2つ取る関数へのポインタの宣言 */


	pfunc2 = printDemo;
	pfunc2("\nPointer Function Call");

	pfunc = add;
	c = pfunc( a, b );
	printf( "c = %d\n" ,c );

	pfunc = sub;
	c = pfunc( a, b );
	printf( "c = %d\n" ,c );

	pfunc = multi;
	c = multi( a, b );
	printf( "c = %d\n" ,c );

}

PRIVATE void ExecPointerFunctionArrayCall( int a, int b, int c )
{
	int i;
	void (*pfunc2)(char*);					/* 返り値がvoid型で、char型ポインタを引数に取る関数へのポインタの宣言 */
	int (*pfuncArray[3])(int, int);			/* 返り値がint型で、int型の引数を2つ取る関数へのポインタで、要素数が3の配列の宣言 */


	pfunc2 = printDemo;
	pfunc2("\nPointer Function Array Call");

	pfuncArray[0] = add;
	pfuncArray[1] = sub;
	pfuncArray[2] = multi;

	for ( i = 0; i < 3; i++ )
	{
		c = pfuncArray[i]( a, b );
		printf( "c = %d\n" ,c );
	}

}

PRIVATE void ExecTypedefPointerFunctionCall( int a, int b, int c )
{
	int i;

	typedef void (FUNC_PRINT)(char*);				/* 返り値がvoid型で、char型ポインタを引数に取る関数に、FUNC_PRINTというは別名を付ける。関数のtypedefの記述方法に注意。　						→ 詳細：https://qiita.com/aminevsky/items/82ecce1d6d8b42d65533 */
	FUNC_PRINT *pfuncPrint = printDemo;
	pfuncPrint("\nTypedef Pointer Function Call");


	typedef void (*P_FUNC_PRINT)(char*);			/* 返り値がvoid型で、char型ポインタを引数に取る関数へのポインタに、P_FUNC_PRINTというは別名を付ける。関数ポインタのtypedefの記述方法に注意。　	→ 詳細：https://qiita.com/aminevsky/items/82ecce1d6d8b42d65533 */
	P_FUNC_PRINT funcPrint = printDemo;
	funcPrint("Typedef Pointer Function Call");



	typedef int (FUNC)(int, int);					/* 返り値がint型で、int型の引数を2つ取る関数に、FUNCという別名を付ける。関数のtypedefの記述方法に注意。 */
	FUNC*  pfunc[3];

	pfunc[0] = add;
	pfunc[1] = sub;
	pfunc[2] = multi;


	typedef int (*P_FUNC)(int, int);				/* 返り値がint型で、int型の引数を2つ取る関数へのポインタに、P_FUNCという別名を付ける。関数ポインタのtypedefの記述方法に注意。 */
	P_FUNC func[3];

	func[0] = add;
	func[1] = sub;
	func[2] = multi;

	for ( i = 0; i < 3; i++ )
	{
		c = pfunc[i]( a, b );
		printf( "c = %d\n" ,c );

		c = func[i]( a, b );
		printf( "c = %d\n" ,c );
	}

}

PRIVATE P_INT_ARRAY GetIntTypeArrayAddr()			/* int型の配列へのポインタ(2重ポインタ)を返す関数。		→参照：https://miyanetdev.com/archives/245 */
{
	return &m_aArray;
}

PRIVATE void ExecTypedefArrayTypeReturnFunctionCall()
{
	int i;
	int (*pArray)[5];								/* 要素数が5であるint型の配列へのポインタの宣言 */

	printDemo("\nTypedef Array Type Return Function Call");

	pArray = GetIntTypeArrayAddr();					/* int型の配列へのポインタを取得する */

	for ( i = 0; i < 5; i++ )
	{
		printf("Array[%d] = %d\n"	,i ,(*pArray)[i] );		/* 演算子の優先順位により、*pArrayを()で括らないと正しい結果にならない */
	}

}

PUBLIC void main()
{
	int a, b, c;

	a = 2;
	b = 3;
	c = 0;

	/* 通常関数呼び出しテスト */
	ExecNormalFunctionCall( a, b, c );


	/* 関数ポインタによる呼び出しテスト */
	ExecPointerFunctionCall( a, b, c );


	/* 関数ポインタ配列による呼び出しテスト */
	ExecPointerFunctionArrayCall( a, b, c );


	/* typedefした関数ポインタによる呼び出しテスト */
	ExecTypedefPointerFunctionCall( a, b, c );


	/* typedefした配列へのポインタを返す関数の呼び出しテスト */
	ExecTypedefArrayTypeReturnFunctionCall();

}
